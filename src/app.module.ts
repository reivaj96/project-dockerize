import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ArticlesController } from './app/articles/controllers/articles.controller';
import { ArticlesService } from './app/articles/services/articles.service';
import * as connectionOptions from './config/typeorm.config';
import { appConfig } from './config/app-config';
import { ArticleRepository } from './app/articles/repositories/article.repository';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true, load: [appConfig] }),
    TypeOrmModule.forRoot(connectionOptions),
    TypeOrmModule.forFeature([ArticleRepository]),
  ],
  controllers: [ArticlesController],
  providers: [ArticlesService],
})
export class AppModule {}
