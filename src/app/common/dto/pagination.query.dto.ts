import { IsOptional, IsPositive } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class PaginationQueryDto {
  @IsOptional()
  @IsPositive()
  @ApiProperty({ type: Number, description: 'limit', required: false })
  limit: number;

  @IsOptional()
  @IsPositive()
  @ApiProperty({ type: Number, description: 'offset', required: false })
  offset: number;
}
