import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity({ name: 'articles' })
export class Article {
  @PrimaryGeneratedColumn()
  readonly id: number;

  @Column()
  title: string;

  @Column({ type: 'text' })
  description: string;
}
