import { Body, Controller, Delete, Get, HttpCode, HttpStatus, Param, Patch, Post, Query } from '@nestjs/common';
import { ApiBody, ApiCreatedResponse, ApiNoContentResponse, ApiOkResponse, ApiParam, ApiTags } from '@nestjs/swagger';
import { ArticlesService } from '../services/articles.service';
import { PaginationQueryDto } from '../../common/dto/pagination.query.dto';
import { CreateArticleDto } from '../dto/create-article.dto';
import { UpdateArticleDto } from '../dto/update-article.dto';
import { Article } from '../entity/article.entity';

@Controller('articles')
@ApiTags('Articles')
export class ArticlesController {
  constructor(private readonly articlesService: ArticlesService) {}

  @Get()
  @ApiOkResponse({ description: 'Article returned' })
  findAll(@Query() paginationQuery: PaginationQueryDto): Promise<Article[]> {
    return this.articlesService.findAll(paginationQuery);
  }

  @Post()
  @ApiCreatedResponse({ description: 'Article created' })
  @ApiBody({ type: CreateArticleDto })
  create(@Body() createCoffeeDto: CreateArticleDto): Promise<Article> {
    return this.articlesService.create(createCoffeeDto);
  }

  @Patch(':id')
  @ApiOkResponse({ description: 'Article updated' })
  @ApiBody({ type: UpdateArticleDto })
  @ApiParam({ name: 'id', type: Number })
  update(@Param('id') id: string, @Body() updateCoffeeDto: UpdateArticleDto): Promise<Article> {
    return this.articlesService.update(id, updateCoffeeDto);
  }

  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiParam({ name: 'id', type: Number })
  @ApiNoContentResponse({ description: 'Article removed' })
  delete(@Param('id') id: string): Promise<void | never> {
    return this.articlesService.remove(id);
  }
}
