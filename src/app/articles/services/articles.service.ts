import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateArticleDto } from '../dto/create-article.dto';
import { UpdateArticleDto } from '../dto/update-article.dto';
import { PaginationQueryDto } from '../../common/dto/pagination.query.dto';
import { ArticleRepository } from '../repositories/article.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { Article } from '../entity/article.entity';

@Injectable()
export class ArticlesService {
  constructor(@InjectRepository(ArticleRepository) private readonly articleRepository: ArticleRepository) {}

  async findOne(id: string): Promise<Article> {
    const article = await this.articleRepository.findOne(id);

    if (!article) {
      throw new NotFoundException();
    }

    return article;
  }

  async create(createArticleDto: CreateArticleDto): Promise<Article> {
    const create = this.articleRepository.create({
      ...createArticleDto,
    });

    return this.articleRepository.save(create);
  }

  findAll(paginationQuery: PaginationQueryDto): Promise<Article[]> {
    const { limit, offset } = paginationQuery;
    return this.articleRepository.find({
      skip: offset ? offset : 1,
      take: limit ? limit : 10,
    });
  }

  async update(id: string, updateArticleDto: UpdateArticleDto): Promise<Article> {
    const existsArticle = await this.articleRepository.preload({
      id: +id,
      ...updateArticleDto,
    });

    if (!existsArticle) {
      throw new NotFoundException();
    }

    return this.articleRepository.save(existsArticle);
  }

  async remove(id: string): Promise<void> {
    const coffee = await this.findOne(id);
    this.articleRepository.remove(coffee);
  }
}
