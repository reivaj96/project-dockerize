export const appConfig = () => ({
  nodeEnv: process.env.NODE_ENV,
  port: process.env.APP_PORT,
});
