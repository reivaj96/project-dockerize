import { config as configEnv } from 'dotenv';
import { ConnectionOptions } from 'typeorm';
import { Article } from '../app/articles/entity/article.entity';

configEnv();

const connectionOptions: ConnectionOptions = {
  type: 'postgres',
  url: `postgresql://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_DATABASE}`,
  synchronize: false,
  entities: [Article],
  migrations: [__dirname + '/../database/migrations/*{.js,.ts}'],
  cli: {
    migrationsDir: 'src/database/migrations',
  },
};

export = connectionOptions;
