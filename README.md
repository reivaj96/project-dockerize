## Project dockerize

## Setup

```bash
# Build application image
$ docker-compose build
# Run project
$ docker-compose up -d
```

## Built With

- Nest.Js
- Typeorm
- PostgreSQL

## Authors

- Javier Corvera
